from django.http import HttpResponse
from django.shortcuts import render
from .models import Product


def index(request):
    products = Product.objects.all()
    return render(request, 'index.html', {'products': products})


def new(request):
    # .all() .get() .filter() .save()
    # return HttpResponse('New Products')
    new_products = Product.objects.get(name="Mango")
    return render(request, 'new_product.html', {'new_products': new_products})


def offer(request):
    offers = Product.objects.all()
    return HttpResponse('Offers')
