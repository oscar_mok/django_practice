from django.shortcuts import render
from .models import Offer


def index(request):
    offers = Offer.objects.all()
    return render(request, 'offer_offer.html', {'offers': offers})
